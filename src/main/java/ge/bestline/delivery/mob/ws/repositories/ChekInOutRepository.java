package ge.bestline.delivery.mob.ws.repositories;

import ge.bestline.delivery.mob.ws.entities.CourierCheckInOut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ChekInOutRepository extends JpaRepository<CourierCheckInOut, Integer> {
    @Query(nativeQuery = true, value = "select * from courier_check_in_out " +
            "where courier_check_in_out.courier_id = ?1 order by id desc limit 1")
    CourierCheckInOut findTopByCourierIdOrderByOperationTimeDesc(Integer id);
}
