package ge.bestline.delivery.mob.ws.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import java.util.Date;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DeliveryDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true)
    private String detailBarCode;
    @ManyToOne(cascade = CascadeType.DETACH)
    private Route route;
    @ManyToOne(cascade = CascadeType.DETACH)
    private Warehouse warehouse;
    @ManyToMany(cascade = CascadeType.DETACH)
    private List<Parcel> parcels;
    @ManyToOne(cascade = CascadeType.DETACH)
    private User user;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTime;
    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private Date createdTime;

    public DeliveryDetail(String detailBarCode, List<Parcel> parcels, User user) {
        this.detailBarCode = detailBarCode;
        this.parcels = parcels;
        this.user = user;
        this.route = user.getRoute();
        this.warehouse = user.getWarehouse();
    }

    @PrePersist
    protected void onCreate() {
        createdTime = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updatedTime = new Date();
    }
}
