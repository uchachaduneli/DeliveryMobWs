package ge.bestline.delivery.mob.ws.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Parcel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @JsonIgnore
    private Integer deleted;
    // 1 - pre inserted with empty values, will be filled after some time
    private Integer prePrinted;
    @Column(unique = true)
    private String barCode;

    private Integer senderId;
    private String senderName;
    private String senderIdentNumber;
    private String senderContactPerson;
    private String senderAddress;
    private String senderPhone;
    @ManyToOne(cascade = CascadeType.DETACH)
    @NotFound(action = NotFoundAction.IGNORE)
    private City senderCity;
    private Integer sendSmsToSender;

    private Integer receiverId;
    private String receiverName;
    private String receiverIdentNumber;
    private String receiverContactPerson;
    private String receiverAddress;
    private String receiverPhone;

    private String deliveredToPers;
    private String deliveredToPersIdent;
    private String deliveredToPersRelativeLevel;
    private String deliveredToPersNote;
    private String deliveredToPersSignature;
    private String deliveredParcelimage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @NotFound(action = NotFoundAction.IGNORE)
    private City receiverCity;
    private Integer sendSmsToReceiver;

    private Integer payerId;
    private Integer payerSide; // 1 sender  2 receiver   3 third side
    private String payerName;
    private String payerIdentNumber;
    private String payerAddress;
    private String payerPhone;
    private String payerContactPerson;
    @ManyToOne(cascade = CascadeType.DETACH)
    @NotFound(action = NotFoundAction.IGNORE)
    private City payerCity;

    @ManyToOne(cascade = CascadeType.DETACH)
    private ParcelStatusReason status;
    private String statusNote; // insert/update operations ar made from deliveryDetails Page

    private String comment;
    private Integer deliveredConfirmation; //1 yes 2 no
    private Integer count;
    private Integer scannedCount;
    private Double weight;
    private Double volumeWeight;
    private Double gadafutvisPrice;
    private Double totalPrice;
    private Integer deliveryType;// 1 mitana misamartze, 2 mikitxva filialshi
    private Integer paymentType;// 1 invoice, 2 cash, 3 card
    @ManyToOne(cascade = CascadeType.DETACH)
    @NotFound(action = NotFoundAction.IGNORE)
    private Services service;
    private Integer packageType;// 1 amanati, 2 paketi
    @ManyToOne(cascade = CascadeType.DETACH)
    @NotFound(action = NotFoundAction.IGNORE)
    private Route route;
    @ManyToOne(cascade = CascadeType.PERSIST, optional = true)
    @JoinColumn(name = "courier_id")
    @NotFound(action = NotFoundAction.IGNORE)
    private User courier;
    @ManyToOne(cascade = CascadeType.DETACH, optional = true)
    @JoinColumn(name = "author_id")
    @NotFound(action = NotFoundAction.IGNORE)
    private User author;
    private Double tariff;
    private String content;
    private Date deliveryTime;
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private Date updatedTime;
    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private Date createdTime;

    public Parcel(ParcelStatusReason status, User courier) {
        this.status = status;
        this.courier = courier;
    }

    public Parcel(String barCode) {
        this.barCode = barCode;
        this.prePrinted = 1;
    }

    @PrePersist
    protected void onCreate() {
        deleted = 2;
        if (status == null) {
            status = new ParcelStatusReason(1);
        }
        createdTime = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updatedTime = new Date();
    }
}
