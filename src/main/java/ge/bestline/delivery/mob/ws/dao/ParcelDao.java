package ge.bestline.delivery.mob.ws.dao;

import ge.bestline.delivery.mob.ws.entities.Parcel;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ParcelDao {
    EntityManager em;

    public ParcelDao(EntityManager em) {
        this.em = em;
    }

    public List<Parcel> findAll(int page, int rowCount, Parcel srchRequest) {
        Map<String, Object> response = new HashMap<>();
        StringBuilder q = new StringBuilder();
        q.append("Select e From ").append(Parcel.class.getSimpleName()).append(" e Where e.deleted =2 ");

        if (srchRequest.getId() != null) {
            q.append(" and e.id =").append(srchRequest.getId());
        }

        if (srchRequest.getCourier() != null) {
            q.append(" and e.courier.id =").append(srchRequest.getCourier().getId());
        }

        if (srchRequest.getStatus() != null) {
            q.append(" and e.status.id =").append(srchRequest.getStatus().getId());
        }

        if (srchRequest.getBarCode() != null) {
            q.append(" and e.barCode= '").append(srchRequest.getBarCode()).append("'");
        }

        TypedQuery<Parcel> query = em.createQuery(q.toString(), Parcel.class);
        List<Parcel> res = query.setFirstResult(page).setMaxResults(rowCount).getResultList();
        return res;
    }

}
