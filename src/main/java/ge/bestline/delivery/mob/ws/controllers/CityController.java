package ge.bestline.delivery.mob.ws.controllers;

import ge.bestline.delivery.mob.ws.entities.City;
import ge.bestline.delivery.mob.ws.repositories.CityRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequestMapping(path = "city")
public class CityController {

    private final CityRepository repo;

    public CityController(CityRepository repo) {
        this.repo = repo;
    }

    @GetMapping
    public Iterable<City> getCities() {
        log.info("Getting Cities");
        return repo.findAll();
    }

}
