package ge.bestline.delivery.mob.ws.repositories;

import ge.bestline.delivery.mob.ws.entities.ParcelStatusReason;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ParcelStatusReasonRepository extends JpaRepository<ParcelStatusReason, Integer> {
    List<ParcelStatusReason> findByStatusIdAndShowInMobail(Integer parcelStatusId, Boolean showInMobile);

    List<ParcelStatusReason> findByStatusIdAndCategoryContainingAndShowInMobail(Integer parcelStatusId, String category, Boolean showInMobile);

    List<ParcelStatusReason> findByCategoryContainingAndShowInMobail(String category, Boolean showInMobile);
}
