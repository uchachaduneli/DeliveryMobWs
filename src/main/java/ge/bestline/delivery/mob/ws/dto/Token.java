package ge.bestline.delivery.mob.ws.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Token {
    public String token;

    public Token(String token) {
        this.token = token;
    }
}

