package ge.bestline.delivery.mob.ws.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.sql.Timestamp;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ParcelStatusHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String reason;
    private String code;
    private Timestamp statusDateTime;
    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private Date createdTime;
    @ManyToOne(cascade = CascadeType.DETACH)
    private Parcel parcel;

    @ManyToOne(cascade = CascadeType.DETACH, optional = true)
    @NotFound(action = NotFoundAction.IGNORE)
    private User operUSer;

    public ParcelStatusHistory(Parcel parcel, String name, String code, String reason, Timestamp statusDateTime, User operUSer) {
        this.parcel = parcel;
        this.reason = reason;
        this.name = name;
        this.code = code;
        this.statusDateTime = statusDateTime;
        this.operUSer = operUSer;
    }

    @PrePersist
    protected void onCreate() {
        createdTime = new Date();
    }

}
