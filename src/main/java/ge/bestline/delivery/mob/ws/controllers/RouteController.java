package ge.bestline.delivery.mob.ws.controllers;

import ge.bestline.delivery.mob.ws.entities.Route;
import ge.bestline.delivery.mob.ws.repositories.RouteRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@RestController
@RequestMapping(path = "/route")
public class RouteController {

    private final RouteRepository repo;

    public RouteController(RouteRepository repo) {
        this.repo = repo;
    }

    @GetMapping(path = "/byCityId/{id}")
    public ResponseEntity<List<Route>> getRoutesByCityId(@PathVariable Integer id) {
        log.info("Getting Route With City ID: " + id);
        return ResponseEntity.ok(repo.findByCityId(id));
    }

}
