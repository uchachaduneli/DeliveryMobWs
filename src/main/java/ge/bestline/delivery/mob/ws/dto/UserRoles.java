package ge.bestline.delivery.mob.ws.dto;

public enum UserRoles {
    ADMIN("ADMIN"),
    COURIER("COURIER"),
    OFFICE("OFFICE"),
    OPERATOR("OPERATOR"),
    MANAGER("MANAGER"),
    DRIVER("DRIVER"),
    CUSTOMER("CUSTOMER");

    private String value;

    UserRoles(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
