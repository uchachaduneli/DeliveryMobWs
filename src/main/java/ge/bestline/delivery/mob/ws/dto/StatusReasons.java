package ge.bestline.delivery.mob.ws.dto;

public enum StatusReasons {
    PP(1), // globalidan axali damatebuli
    RG(2), // ofisma marshruti(kurieri) miaba
    SE(4), // kurierma naxa
    PU(8), // kurierma aigo an ofisshi mivida klienti da portalidan daamata ofisis tanamshromelma

    WC(11); // gzavnili sakuriero xazzea

    private Integer value;

    StatusReasons(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
