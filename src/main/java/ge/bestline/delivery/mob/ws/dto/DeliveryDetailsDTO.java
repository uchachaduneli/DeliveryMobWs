package ge.bestline.delivery.mob.ws.dto;

import ge.bestline.delivery.mob.ws.entities.Parcel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryDetailsDTO {
    private Integer userId;
    private List<Parcel> parcels;
}
