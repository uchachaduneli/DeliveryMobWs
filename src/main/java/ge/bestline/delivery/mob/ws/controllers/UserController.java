package ge.bestline.delivery.mob.ws.controllers;

import ge.bestline.delivery.mob.ws.Exception.ResourceNotFoundException;
import ge.bestline.delivery.mob.ws.entities.Route;
import ge.bestline.delivery.mob.ws.entities.User;
import ge.bestline.delivery.mob.ws.repositories.RouteRepository;
import ge.bestline.delivery.mob.ws.repositories.UserRepository;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@RestController
@RequestMapping(path = "/user")
public class UserController {

    private final RouteRepository routeRepo;
    private final UserRepository userRepo;

    public UserController(RouteRepository routeRepo,
                          UserRepository userRepo) {
        this.routeRepo = routeRepo;
        this.userRepo = userRepo;
    }

    @PutMapping(path = "/{userId}/{routeId}/{printerMacAddress}/{token}")
    @Operation(summary = "Update User Route", operationId = "updateById")
    @Transactional
    public ResponseEntity<User> updateById(@PathVariable Integer userId,
                                           @PathVariable Integer routeId,
                                           @PathVariable(required = false) String printerMacAddress,
                                           @PathVariable(required = false) String token) {
        log.info("Updating User printer mac & firebase token");
        // tokeni devaisze igive rcheba tu reinstali an clear data ar gaketda aplikaciaze
        // da tu devaisi axla sxva kurierma aigo wina kuriers tokeni unda waeshalos imito ro
        // tu wina kurierze gaigzavneba rame axal kuriers miuva, devaisze tokeni igive iqneba
        if (token != null && token.length() > 0) {
            List<User> usersWithSameFirebaseToken = userRepo.findByIdNotAndFirebaseToken(userId, token);
            if (usersWithSameFirebaseToken != null && !usersWithSameFirebaseToken.isEmpty()) {
                for (User u : usersWithSameFirebaseToken) {
                    u.setFirebaseToken(null);
                }
                userRepo.saveAll(usersWithSameFirebaseToken);
            }
        }
        Route route = routeRepo.findById(routeId)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find Route Record Using This ID : " + routeId));
        User user = userRepo.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find User Record Using This ID : " + userId));
        log.info("Users Old Route: " + (user.getRoute() != null ? user.getRoute().getName() : "")
                + " New Route: " + route.getName());
        user.setPrinterMacAddress(printerMacAddress);
        user.setRoute(route);
        user.setFirebaseToken(token);
        return ResponseEntity.ok(userRepo.save(user));
    }

}
