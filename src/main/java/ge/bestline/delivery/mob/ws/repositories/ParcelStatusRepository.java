package ge.bestline.delivery.mob.ws.repositories;

import ge.bestline.delivery.mob.ws.entities.ParcelStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ParcelStatusRepository extends JpaRepository<ParcelStatus, Integer> {
    List<ParcelStatus> findByCategoryContaining(String category);
}
