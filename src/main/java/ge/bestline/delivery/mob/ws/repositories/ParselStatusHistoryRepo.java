package ge.bestline.delivery.mob.ws.repositories;

import ge.bestline.delivery.mob.ws.entities.ParcelStatusHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ParselStatusHistoryRepo extends JpaRepository<ParcelStatusHistory, Integer> {
    List<ParcelStatusHistory> findByParcelId(Integer id);
}
