package ge.bestline.delivery.mob.ws.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MultipleDeliveryDTO extends DeliveredParcelInfolDTO {
    private List<BarcodWithScannsCount> barCodes; // {barcode, scannedCount} pairs
}
