package ge.bestline.delivery.mob.ws.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SingleDeliverylDTO extends DeliveredParcelInfolDTO{
    private String barcode;
}
