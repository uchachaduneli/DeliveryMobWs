package ge.bestline.delivery.mob.ws.dto;

import lombok.Data;

@Data
public class BarcodWithScannsCount {
    String barCode;
    Integer scannedCount;
}
