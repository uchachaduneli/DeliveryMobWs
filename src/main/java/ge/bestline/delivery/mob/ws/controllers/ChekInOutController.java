package ge.bestline.delivery.mob.ws.controllers;

import ge.bestline.delivery.mob.ws.Exception.ResourceNotFoundException;
import ge.bestline.delivery.mob.ws.entities.CourierCheckInOut;
import ge.bestline.delivery.mob.ws.repositories.ChekInOutRepository;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RestController
@RequestMapping(path = "/courierInOut")
public class ChekInOutController {

    private final ChekInOutRepository repo;

    public ChekInOutController(ChekInOutRepository repo) {
        this.repo = repo;
    }

    @PostMapping
    @Operation(summary = "Add New Registered IN/OUT Operation for user",
            operationId = "addNew")
    @Transactional
    public ResponseEntity<Object> addNew(@RequestBody CourierCheckInOut obj) {
        log.info("Adding Courier CheckOUT Or CheckIN: " + obj.toString());
        CourierCheckInOut userLastOperation = repo.findTopByCourierIdOrderByOperationTimeDesc(obj.getCourier().getId());
        if (userLastOperation != null && obj.getCarNumber().equalsIgnoreCase(userLastOperation.getCarNumber())) {
            if (obj.getOdometer() < userLastOperation.getOdometer()) {
                log.warn("CANCELLING OPERATION !!! Incorrect Odometer Data");
                return ResponseEntity.internalServerError().body("Odometer can't be less than was at previous time");
            }
            if (!obj.isChekIn() && !userLastOperation.isChekIn()) {
                log.warn("CANCELLING OPERATION !!! Adding CHECKOUT Again after CHEKOUT");
                return ResponseEntity.internalServerError().body("You ALready Have Unclosed CHEKOUT Operation. " +
                        "Please Make CHEKIN Operation At First");
            }
            if (obj.isChekIn() && userLastOperation.isChekIn()) {
                log.warn("CANCELLING OPERATION !!! Adding CHEKIN Again after CHEKIN");
                return ResponseEntity.internalServerError().body("You ALready Have Unclosed CHECKIN Operation. " +
                        "Please Make CHEKOUT Operation At First");
            }
            if (obj.isChekIn()) {
                log.info("calculating distance since previous checkout");
                obj.setDistance(obj.getOdometer() - userLastOperation.getOdometer());
            }
        }
        return ResponseEntity.ok(repo.save(obj));
    }

    @GetMapping(path = "/userLasOperation/{id}")
    @Operation(summary = "Returns Last Registered IN/OUT Operation for user",
            operationId = "getUserLasOperation")
    public ResponseEntity<CourierCheckInOut> getUserLasOperation(@PathVariable Integer id) {
        log.info("Getting last operation from CourierCheckInOut By courier ID: " + id);
        return ResponseEntity.ok(repo.findTopByCourierIdOrderByOperationTimeDesc(id));
    }

    @PutMapping(path = "/{id}")
    @Transactional
    public ResponseEntity<CourierCheckInOut> updateById(@PathVariable Integer id, @RequestBody CourierCheckInOut request) {
        log.info("Updating Parcel");
        CourierCheckInOut existing = repo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Can't find Record Using This ID : " + id));
        log.info("Old Values: " + existing.toString() + "    New Values: " + request.toString());

        existing.setCarNumber(request.getCarNumber());
        existing.setOdometer(request.getOdometer());
        existing.setOperationTime(request.getOperationTime());

        CourierCheckInOut updatedObj = repo.save(existing);
        return ResponseEntity.ok(updatedObj);
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Returns IN/OUT Record By ID",
            operationId = "getById")
    public CourierCheckInOut getById(@PathVariable Integer id) {
        log.info("Getting CourierCheckInOut With ID: " + id);
        return repo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Can't find Record Using This ID " + id));
    }

}
