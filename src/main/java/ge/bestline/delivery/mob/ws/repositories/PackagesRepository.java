package ge.bestline.delivery.mob.ws.repositories;

import ge.bestline.delivery.mob.ws.entities.Packages;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PackagesRepository extends JpaRepository<Packages, Integer> {
    List<Packages> findByParcelId(Integer id);
}
