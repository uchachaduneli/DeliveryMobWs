package ge.bestline.delivery.mob.ws.repositories;

import ge.bestline.delivery.mob.ws.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface UserRepository extends JpaRepository<User, Integer> {

    @Query(nativeQuery = true, value = "select u.* from user_role " +
            "join user u on u.id = user_role.user_id " +
            "where role_name in ?3 and u.user_name = ?1 and u.password= ?2 group by user_id")
    User findByUserPassAndRoleNameIn(String username, String pass, Set<String> roles);

    List<User> findByIdNotAndFirebaseToken(Integer userId, String token);

    User findByUserName(String username);
}
