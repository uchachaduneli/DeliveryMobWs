package ge.bestline.delivery.mob.ws.controllers;

import ge.bestline.delivery.mob.ws.Exception.ResourceNotFoundException;
import ge.bestline.delivery.mob.ws.dao.ParcelDao;
import ge.bestline.delivery.mob.ws.dto.*;
import ge.bestline.delivery.mob.ws.entities.*;
import ge.bestline.delivery.mob.ws.repositories.*;
import ge.bestline.delivery.mob.ws.security.jwt.JwtTokenProvider;
import ge.bestline.delivery.mob.ws.services.BarCodeService;
import ge.bestline.delivery.mob.ws.util.CharacterTranslator;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.servlet.http.HttpServletRequest;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping(path = "/parcel")
public class ParcelController {

    @Value("${uploadsPath}")
    private String uploadsPath;
    private final ParcelRepository repo;
    private final PackagesRepository packagesRepo;
    private final ParcelDao dao;
    private final ParselStatusHistoryRepo statusHistoryRepo;
    private final ParcelStatusReasonRepository statusReasonRepo;
    private final UserRepository userRepository;
    private final DeliveryDetailsRepository deliveryDetailsRepo;
    private final JwtTokenProvider jwtTokenProvider;
    private final BarCodeService barCodeService;
    private final CharacterTranslator translator;

    public ParcelController(ParcelRepository repo,
                            PackagesRepository packagesRepo,
                            ParcelDao dao, ParselStatusHistoryRepo statusHistoryRepo,
                            ParcelStatusReasonRepository statusReasonRepo,
                            UserRepository userRepository,
                            DeliveryDetailsRepository deliveryDetailsRepo,
                            JwtTokenProvider jwtTokenProvider, BarCodeService barCodeService,
                            CharacterTranslator translator) {
        this.repo = repo;
        this.packagesRepo = packagesRepo;
        this.dao = dao;
        this.statusHistoryRepo = statusHistoryRepo;
        this.statusReasonRepo = statusReasonRepo;
        this.userRepository = userRepository;
        this.deliveryDetailsRepo = deliveryDetailsRepo;
        this.jwtTokenProvider = jwtTokenProvider;
        this.barCodeService = barCodeService;
        this.translator = translator;
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleParcelControllerExceptions(Exception ex) {
        log.error(ex.getMessage(), ex);
        if (ex instanceof ResourceNotFoundException) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping(path = "/printStiker/{id}/{printcount}")
    @Operation(summary = "Returns FullFilled Parcel Stiker Print Template, Params: ParcelId, Prints Count",
            operationId = "getPrintString")
    public String getPrintString(@PathVariable Integer id, @PathVariable Integer printcount) {
        String template = "";
        log.info("Getting Print Template String For Parcel With ID: " + id);
        Parcel p = repo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Can't find Record Using This ID : " + id));
        for (int i = 0; i < printcount; i++) {
            template += loadPrinterTemplateFromFile()
                    .replaceAll("senderCity", p.getSenderCity() != null ? translator.ka(p.getSenderCity().getName()) : "")
                    .replaceAll("sndrCityCode", p.getSenderCity() != null ? p.getSenderCity().getCode() : "")
                    .replaceAll("receiverCity", p.getReceiverCity() != null ? translator.ka(p.getReceiverCity().getName()) : "")
                    .replaceAll("rcvrCityCode", p.getReceiverCity() != null ? p.getReceiverCity().getCode() : "")
                    .replace("pageIndex", i + 1 + " / " + printcount)
                    .replace("senderIdent", Optional.ofNullable(p.getSenderIdentNumber()).orElse(""))
                    .replace("senderName", translator.ka(p.getSenderName()))
                    .replace("senderAddress", translator.ka(Optional.ofNullable(p.getSenderAddress()).orElse("")))
                    .replace("senderPhone ", Optional.ofNullable(p.getSenderPhone()).orElse(""))
                    .replace("senderContactPerson", translator.ka(Optional.ofNullable(p.getSenderContactPerson()).orElse("")))
                    .replace("receiverIdent", Optional.ofNullable(p.getReceiverIdentNumber()).orElse(""))
                    .replace("receiverName", translator.ka(Optional.ofNullable(p.getReceiverName()).orElse("")))
                    .replace("receiverAddress", translator.ka(Optional.ofNullable(p.getReceiverAddress()).orElse("")))
                    .replace("receiverPhone ", Optional.ofNullable(p.getReceiverPhone()).orElse(""))
                    .replace("receiverContactPerson ", translator.ka(Optional.ofNullable(p.getReceiverContactPerson()).orElse("")))
                    .replace("serviceName", p.getService() != null ? translator.ka(p.getService().getName()) : "")
                    .replace("paymentType", getPaymentTypeName(p.getPaymentType()))
                    .replace("comment", translator.ka(Optional.ofNullable(p.getComment()).orElse("")))
                    .replace("weight", Optional.ofNullable(p.getWeight()).orElse(0.0) + "")
                    .replace("price", Optional.ofNullable(p.getTotalPrice()).orElse(0.0) + "")
                    .replace("barcode", Optional.ofNullable(p.getBarCode()).orElse(""));
        }

        return template;
    }

    private String getPaymentTypeName(Integer val) {
        if (val != null) {
            switch (val) {
                case 1:
                    return "gadaricxva";
                case 2:
                    return "naRdi";
                case 3:
                    return "baraTi";
                default:
                    return "-";
            }
        } else {
            return "-";
        }
    }

    @SneakyThrows
    public String loadPrinterTemplateFromFile() {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("printTemplate.txt").getFile());
        String data = FileUtils.readFileToString(file, "UTF-8");
        return data;
        //new String(Files.readAllBytes(Paths.get("src/main/resources/printTemplate.txt")));
    }

    @GetMapping(path = "/statusHistory/{id}")
    @Operation(summary = "Returns Parcel Updates History",
            operationId = "getParcelStatusHistoryByParcelId")
    public ResponseEntity<List<ParcelStatusHistory>> getParcelStatusHistoryByParcelId(@PathVariable Integer id) {
        log.info("Getting ParcelStatusHistory By Parcel ID: " + id);
        return ResponseEntity.ok(statusHistoryRepo.findByParcelId(id));
    }

    @PutMapping(path = "/{id}")
    @Operation(summary = "Update Parcel Details", operationId = "updateById")
    @Transactional
    public ResponseEntity<Parcel> updateById(@PathVariable Integer id,
                                             @RequestBody Parcel request
            , HttpServletRequest req) {
        TokenUser requester = jwtTokenProvider.getRequesterUserData(req);
        log.info("Updating Parcel by User ", requester.toString());
        Parcel existing = repo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find Record Using This ID : " + id));
        log.info("Old Values: " + existing.toString() + "    New Values: " + request.toString());
        if (request.getStatus().getId() != existing.getStatus().getId()) {
            ParcelStatusReason status = statusReasonRepo.findById(request.getStatus().getId())
                    .orElseThrow(() -> new ResourceNotFoundException("Can't find Status Using This ID : " + request.getStatus().getId()));
            saveStatusHistory(existing, status, requester.getId());
            existing.setStatus(request.getStatus());
        }
        existing.setCount(request.getCount());
        existing.setWeight(request.getWeight());
        existing.setPaymentType(request.getPaymentType());

        Parcel updatedObj = repo.save(existing);
        return ResponseEntity.ok(updatedObj);
    }

    @PutMapping(path = "/status/{parcelId}/{statusId}")
    @Operation(summary = "Update Parcel Status By id", operationId = "updateStatusById")
    @Transactional
    public ResponseEntity<Parcel> updateStatusById(@PathVariable Integer parcelId,
                                                   @PathVariable Integer statusId,
                                                   HttpServletRequest req) {
        log.info("Updating Parcel's status");
        TokenUser requester = jwtTokenProvider.getRequesterUserData(req);
        Parcel existing = repo.findById(parcelId)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find Parcel Using This ID : " + parcelId));
        ParcelStatusReason statusReason = statusReasonRepo.findById(statusId)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find Status Using This ID : " + statusId));
        log.info(existing.getBarCode() + " Parcel's Old status: " + existing.getStatus().toString() + "    New status: " + statusReason.toString());
        if (statusReason.getId() != existing.getStatus().getId()) {

            saveStatusHistory(existing, statusReason, requester.getId());
            existing.setStatus(statusReason);
        }
        Parcel updatedObj = repo.save(existing);
        return ResponseEntity.ok(updatedObj);
    }

    @PutMapping(path = "/multiplesStatusUpdate/{statusId}")
    @Operation(summary = "Update Parcel Status By id", operationId = "updateMultiplesStatusById")
    @Transactional
    public ResponseEntity<List<Parcel>> updateMultiplesStatusById(@PathVariable Integer statusId,
                                                                  @RequestBody MultipleDeliveryDTO request,
                                                                  HttpServletRequest req) {
        log.info("Updating Parcel's status");
        TokenUser requester = jwtTokenProvider.getRequesterUserData(req);
        User user = userRepository.findById(requester.getId()).orElseThrow(() -> new ResourceNotFoundException("Can't find User By Requester ID : " + requester.getId()));
        List<Parcel> foundedParcels = repo.findByBarCodeIn(request.getBarCodes().stream().map(BarcodWithScannsCount::getBarCode).collect(Collectors.toList()));
        ParcelStatusReason statusReason = statusReasonRepo.findById(statusId)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find Status Using This ID : " + statusId));

        List<ParcelStatusHistory> histories = new ArrayList<>();
        String statusChangeLogTxt = "changing statuses to " + statusReason.getStatus().getCode() + ", for these parcels: ";
        for (Parcel p : foundedParcels) {
            statusChangeLogTxt += p.getBarCode() + " - " + p.getStatus().getStatus().getCode();
            histories.add(new ParcelStatusHistory(p
                    , statusReason.getStatus().getName()
                    , statusReason.getStatus().getCode()
                    , statusReason.getName()
                    , new Timestamp(new Date().getTime())
                    , user
            ));
            p.setStatus(statusReason);
            p.setScannedCount(getFilteredsScannedCount(p.getBarCode(), request.getBarCodes()));
        }
        if (foundedParcels != null && !foundedParcels.isEmpty()) {
            log.warn(statusChangeLogTxt);
        }
        statusHistoryRepo.saveAll(histories);
        repo.saveAll(foundedParcels);

        return ResponseEntity.ok(foundedParcels);
    }

    private Integer getFilteredsScannedCount(String barCode, List<BarcodWithScannsCount> barCodesListWithScanCounts) {
        List<BarcodWithScannsCount> founded = barCodesListWithScanCounts.stream().filter(o -> barCode.equals(o.getBarCode())).collect(Collectors.toList());
        if (founded.isEmpty())
            return null;
        return founded.get(0).getScannedCount();
    }

    private void saveStatusHistory(Parcel existing, ParcelStatusReason status, Integer requesterId) {
        statusHistoryRepo.save(new ParcelStatusHistory(existing
                , status.getStatus().getName()
                , status.getStatus().getCode()
                , status.getName()
                , new Timestamp(new Date().getTime())
                , new User(requesterId)
        ));
    }

    @PutMapping(path = "/courierSeenStatus/{id}/{status}")
    @Operation(summary = "Update Parcel's Courier Seen Status", operationId = "updateCourierSeenStatus")
    @Transactional
    public ResponseEntity<Parcel> updateCourierSeenStatus(@PathVariable Integer id,
                                                          @PathVariable Integer status,
                                                          HttpServletRequest req) {
        log.info("Updating Parcel's Courier Seen Status");
        TokenUser requester = jwtTokenProvider.getRequesterUserData(req);
        Parcel existing = repo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Can't find Parcel Record Using This ID : " + id));
        log.info("Old status: " + existing.getStatus().getName() + "    New Status: " + status);
        ParcelStatusReason statusReason = statusReasonRepo.findById(courierStatusIntToParcelStatus(status).getId())
                .orElseThrow(() -> new ResourceNotFoundException("Can't find Status Using This ID : " + courierStatusIntToParcelStatus(status).getId()));
        saveStatusHistory(existing, statusReason, requester.getId());
        existing.setStatus(statusReason);
        Parcel updatedObj = repo.save(existing);
        return ResponseEntity.ok(updatedObj);
    }

    private ParcelStatusReason courierStatusIntToParcelStatus(Integer couierStatus) {
        ParcelStatusReason ps = null;
        switch (couierStatus) {
            case 1:
                ps = new ParcelStatusReason(StatusReasons.RG.getValue());
                break;
            case 2:
                ps = new ParcelStatusReason(StatusReasons.SE.getValue());
                break;
            case 3:
                ps = new ParcelStatusReason(StatusReasons.PU.getValue());
                break;
            default:
                break;
        }
        return ps;
    }

    @GetMapping(path = "/userParcels/{mobUserId}/{statusId}")
    @Operation(summary = "Returns parcels for user, You can fIlter with status id 1-NEW / 2-SEEN / 3-TAKEN",
            operationId = "getByUserId")
    public ResponseEntity<List<Parcel>> getByUserId(@PathVariable Integer mobUserId, @PathVariable Integer statusId) {//1 axali, 2 nanaxi, 3 agebuli
        return new ResponseEntity<>(dao.findAll(0, 100,
                new Parcel(courierStatusIntToParcelStatus(statusId), new User(mobUserId))), HttpStatus.OK);
    }

    @GetMapping(path = "/checkByBarcode/{barcode}")
    @Operation(summary = "Searchs and returns parcels by barcode",
            operationId = "checkIfExistsByBarcode")
    public ResponseEntity<Parcel> checkIfExistsByBarcode(@PathVariable String barcode) {
        Parcel existing = repo.findByBarCode(barcode)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find Parcel Record Using This BarCode : " + barcode));
        return new ResponseEntity<>(existing, HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public Parcel getById(@PathVariable Integer id) {
        log.info("Getting Parcel With ID: " + id);
        return repo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Can't find Record Using This ID " + id));
    }

    @GetMapping(path = "/package/{id}")
    public ResponseEntity<List<Packages>> getPackagesByParcelId(@PathVariable Integer id) {
        log.info("Getting Packages By Parcel ID: " + id);
        return ResponseEntity.ok(packagesRepo.findByParcelId(id));
    }

    @PostMapping("/deliveryDetails")
    @Operation(summary = "Save Delivered Parcel Lists Details",
            operationId = "saveDeliveryDetailsInfo")
    @Transactional
    public ResponseEntity<DeliveryDetail> saveDeliveryDetailsInfo(@RequestBody DeliveryDetailsDTO request) {
        log.info("Saving multiple Parcel's Delivery Details Info");
        User user = userRepository.findById(request.getUserId()).orElseThrow(
                () -> new ResourceNotFoundException("Can't find User Using This ID : " + request.getUserId()));
        String barcode = barCodeService.getBarcodes(1).get(0);
        if (deliveryDetailsRepo.findByDetailBarCode(barcode).isPresent()) {
            barcode = barCodeService.getBarcodes(1).get(0);
        }
        List<Parcel> foundedParcels = repo.findByBarCodeIn(request.getParcels().stream().map(Parcel::getBarCode)
                .collect(Collectors.toList()));
        List<Parcel> requestedSortParcels = new ArrayList<>();
        for (Parcel rp : request.getParcels()) {
            for (Parcel fp : foundedParcels) {
                if (fp.getBarCode().equals(rp.getBarCode())) {
                    requestedSortParcels.add(fp);
                }
            }
        }
        foundedParcels = requestedSortParcels;
        DeliveryDetail det = deliveryDetailsRepo.save(new DeliveryDetail(barcode, foundedParcels, user));
        ParcelStatusReason status = statusReasonRepo.findById(StatusReasons.WC.getValue())
                .orElseThrow(() -> new ResourceNotFoundException("Can't find Status Using ENUMS WC value"));
        List<ParcelStatusHistory> histories = new ArrayList<>();
        String statusChangeLogTxt = "changing statuses to WC, for these parcels: ";
        for (Parcel p : det.getParcels()) {
            statusChangeLogTxt += p.getBarCode() + ", ";
            histories.add(new ParcelStatusHistory(p
                    , status.getStatus().getName()
                    , status.getStatus().getCode()
                    , status.getName()
                    , new Timestamp(new Date().getTime())
                    , new User(request.getUserId())
            ));
            p.setStatus(status);
            // update parcels scannCount from requested value
            List<Parcel> founded = request.getParcels().stream().filter(o -> p.getBarCode().equals(o.getBarCode())).collect(Collectors.toList());
            if (!founded.isEmpty()) {
                p.setScannedCount(founded.get(0).getScannedCount());
            }
        }
        if (det.getParcels() != null && !det.getParcels().isEmpty()) {
            log.warn(statusChangeLogTxt);
        }
        statusHistoryRepo.saveAll(histories);
        repo.saveAll(det.getParcels());
        return ResponseEntity.ok(det);
    }

    @GetMapping("/deliveryDetails/{userId}")
    @Operation(summary = "Get User's Current Day Delivery Details Parcels List",
            operationId = "getDeliveryDetailsInfo")
    @Transactional
    public ResponseEntity<List<Parcel>> saveDeliveryDetailsInfo(@PathVariable Integer userId) {
        log.info("Saving multiple Parcel's Delivery Details Info");
        User user = userRepository.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException("Can't find User Using This ID : " + userId));

        SimpleDateFormat dateFrmt = new SimpleDateFormat("YYYY-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 1);

        List<Parcel> det = repo.findUsersCurrDayDeliveryDetailsNonDeliveredParcels(user.getId(), dateFrmt.format(new Date()), dateFrmt.format(c.getTime()));
        return ResponseEntity.ok(det);
    }

    @PostMapping("/singleDelivery")
    @Operation(summary = "Save Single Parcels Delivery Info & Receiver Data",
            operationId = "saveSingleDeliveryInfo")
    @Transactional
    public ResponseEntity<Parcel> saveSingleDeliveryInfo(@RequestBody SingleDeliverylDTO request
            , HttpServletRequest req) {
        log.info("Updating single Parcel's Delivery Info, Receiver Data and Status");
        TokenUser requester = jwtTokenProvider.getRequesterUserData(req);
        Parcel existing = repo.findByBarCode(request.getBarcode())
                .orElseThrow(() -> new ResourceNotFoundException("Can't find Parcel Record Using This BarCode : " + request.getBarcode()));
        log.info("Old status: " + existing.getStatus().getId() + "    New Status: " + request.getStatusReasonId());
        ParcelStatusReason statusReason = statusReasonRepo.findById(request.getStatusReasonId())
                .orElseThrow(() -> new ResourceNotFoundException("Can't find Status Using This ID : " + request.getStatusReasonId()));
        saveStatusHistory(existing, statusReason, requester.getId());
        existing.setStatus(statusReason);
        existing.setDeliveredToPers(request.getDeliveredToPers());
        existing.setDeliveredToPersIdent(request.getDeliveredToPersIdent());
        existing.setDeliveredToPersRelativeLevel(request.getDeliveredToPersRelativeLevel());
        existing.setDeliveredToPersNote(request.getDeliveredToPersNote());
        if (request.getDeliveredToPersSignatureBase64() != null && request.getDeliveredToPersSignatureBase64().length() > 10) {
            existing.setDeliveredToPersSignature(convertBase64ToImage(request.getDeliveredToPersSignatureBase64(), existing.getBarCode(), true));
        }
        if (request.getDeliveredParcelimageBase64() != null && request.getDeliveredParcelimageBase64().length() > 10) {
            existing.setDeliveredParcelimage(convertBase64ToImage(request.getDeliveredParcelimageBase64(), existing.getBarCode(), false));
        }
        Parcel updatedObj = repo.save(existing);
        return ResponseEntity.ok(updatedObj);
    }

    @PostMapping("/multipleDelivery")
    @Operation(summary = "Mark Parcels Delivered, Saves Receiver Data & Some Info",
            operationId = "saveMultipleDeliveryInfo")
    @Transactional
    public ResponseEntity<List<Parcel>> saveMultipleDeliveryInfo(@RequestBody MultipleDeliveryDTO request
            , HttpServletRequest req) {
        log.info("Updating single Parcel's Delivery Info, Receiver Data and Status");
        TokenUser requester = jwtTokenProvider.getRequesterUserData(req);
        List<Parcel> existingsList = new ArrayList<>();
        for (String barCode : request.getBarCodes().stream().map(BarcodWithScannsCount::getBarCode).collect(Collectors.toList())) {
            Parcel existing = repo.findByBarCode(barCode)
                    .orElseThrow(() -> new ResourceNotFoundException("Can't find Parcel Record Using This BarCode : " + barCode));
            ParcelStatusReason statusReason = statusReasonRepo.findById(request.getStatusReasonId())
                    .orElseThrow(() -> new ResourceNotFoundException("Can't find Status Using This ID : " + request.getStatusReasonId()));
            saveStatusHistory(existing, statusReason, requester.getId());
            existing.setScannedCount(getFilteredsScannedCount(existing.getBarCode(), request.getBarCodes()));
            existing.setStatus(statusReason);
            existing.setDeliveredToPers(request.getDeliveredToPers());
            existing.setDeliveredToPersIdent(request.getDeliveredToPersIdent());
            existing.setDeliveredToPersRelativeLevel(request.getDeliveredToPersRelativeLevel());
            existing.setDeliveredToPersNote(request.getDeliveredToPersNote());
            if (request.getDeliveredToPersSignatureBase64() != null && request.getDeliveredToPersSignatureBase64().length() > 10) {
                existing.setDeliveredToPersSignature(convertBase64ToImage(request.getDeliveredToPersSignatureBase64(), existing.getBarCode(), true));
            }
            if (request.getDeliveredParcelimageBase64() != null && request.getDeliveredParcelimageBase64().length() > 10) {
                existing.setDeliveredParcelimage(convertBase64ToImage(request.getDeliveredParcelimageBase64(), existing.getBarCode(), false));
            }
            existingsList.add(existing);
        }
        return ResponseEntity.ok(repo.saveAll(existingsList));
    }

    @SneakyThrows
    private String convertBase64ToImage(String base64Str, String barcode, boolean isSignature) {
        byte[] decodedBytes = Base64.getDecoder().decode(base64Str.split(",")[1]);
        String outputFileName = uploadsPath + "/deliveredImages/"
                + barcode + (isSignature ? "_Signature.png" : "_Parcel.png");
        FileUtils.writeByteArrayToFile(new File(outputFileName), decodedBytes);
        log.info((isSignature ? "Signature Uploaded To Server: " : "Parcel Image Uploaded To server: ") + outputFileName);
        return barcode + (isSignature ? "_Signature.png" : "_Parcel.png");
    }
}
