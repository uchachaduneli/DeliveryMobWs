package ge.bestline.delivery.mob.ws.repositories;

import ge.bestline.delivery.mob.ws.entities.Parcel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ParcelRepository extends JpaRepository<Parcel, Integer> {
    Optional<Parcel> findByBarCode(String barcode);

    List<Parcel> findByBarCodeIn(List<String> list);

    @Query(nativeQuery = true, value = "select p.* " +
            "from delivery_detail_parcels ddp " +
            "         inner join deliverydb.delivery_detail d on ddp.delivery_detail_id = d.id " +
            "         inner join deliverydb.parcel p on ddp.parcels_id = p.id " +
            "where d.user_id=?1 and ( d.created_time between ?2 and ?3) " +
            "  and p.status_id not in (13, 14, 15, 16, 17, 18, 19, 20)")
    List<Parcel> findUsersCurrDayDeliveryDetailsNonDeliveredParcels(Integer userId, String fromDate, String toDate);
}
