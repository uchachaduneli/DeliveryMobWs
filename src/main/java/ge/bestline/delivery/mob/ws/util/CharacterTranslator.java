package ge.bestline.delivery.mob.ws.util;

import org.springframework.stereotype.Component;

@Component
public class CharacterTranslator {
    public String ka(String str) {
        if (str != null) {
            return str.replaceAll("ღ", "R").replaceAll("ჯ", "j").
                    replaceAll("უ", "u").replaceAll("კ", "k").
                    replaceAll("ე", "e").replaceAll("ნ", "n").
                    replaceAll("გ", "g").replaceAll("შ", "S").
                    replaceAll("წ", "w").replaceAll("ზ", "z").
                    replaceAll("ფ", "f").replaceAll("ძ", "Z").
                    replaceAll("ვ", "v").replaceAll("ა", "a").
                    replaceAll("პ", "p").replaceAll("რ", "r").
                    replaceAll("ო", "o").replaceAll("ლ", "l").
                    replaceAll("ჭ", "W").replaceAll("ჩ", "C").
                    replaceAll("ყ", "y").replaceAll("ს", "s").
                    replaceAll("მ", "m").replaceAll("ი", "i").
                    replaceAll("ტ", "t").replaceAll("ქ", "q").
                    replaceAll("ბ", "b").replaceAll("ჰ", "h").
                    replaceAll("დ", "d").replaceAll("ჟ", "J").
                    replaceAll("ხ", "x").replaceAll("ც", "c").
                    replaceAll("თ", "T");
        } else {
            return "-";
        }
    }
}
