package ge.bestline.delivery.mob.ws.controllers;


import ge.bestline.delivery.mob.ws.entities.ParcelStatus;
import ge.bestline.delivery.mob.ws.entities.ParcelStatusReason;
import ge.bestline.delivery.mob.ws.repositories.ParcelStatusReasonRepository;
import ge.bestline.delivery.mob.ws.repositories.ParcelStatusRepository;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@RestController
@RequestMapping(path = "/parcelStatus")
public class ParcelStatusController {

    private final ParcelStatusRepository repo;
    private final ParcelStatusReasonRepository statusReasonRepo;

    public ParcelStatusController(ParcelStatusRepository repo,
                                  ParcelStatusReasonRepository statusReasonRepo) {
        this.repo = repo;
        this.statusReasonRepo = statusReasonRepo;
    }

    @GetMapping
    @Operation(summary = "Returns All Parcel Statuses",
            operationId = "getAll")
    public ResponseEntity<List<ParcelStatus>> getAll() {
        return new ResponseEntity<>(repo.findByCategoryContaining("კურიერ"), HttpStatus.OK);
    }

    @GetMapping(path = "/statusReason/{parcelStatusId}")
    @Operation(summary = "Returns All Parcel Status Reasons for Specific Status ID",
            operationId = "getByParcelStatusId")
    public ResponseEntity<List<ParcelStatusReason>> getByParcelStatusId(@PathVariable Integer parcelStatusId) {
        log.info("Getting Parcel Statuse Reason With Parcel Status ID: " + parcelStatusId);
        return new ResponseEntity<>(statusReasonRepo.findByStatusIdAndCategoryContainingAndShowInMobail(parcelStatusId, "კურიერ", true), HttpStatus.OK);
    }

    @GetMapping(path = "/statusReason")
    @Operation(summary = "Returns All Parcel Status Reasons", operationId = "getAllStatusReasons")
    public ResponseEntity<List<ParcelStatusReason>> getAllStatusReasons() {
        return new ResponseEntity<>(statusReasonRepo.findByCategoryContainingAndShowInMobail("კურიერ", true), HttpStatus.OK);
    }

}
