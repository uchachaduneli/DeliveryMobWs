package ge.bestline.delivery.mob.ws.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeliveredParcelInfolDTO {
    private Integer statusId;
    private Integer statusReasonId;
    private String deliveredToPers;
    private String deliveredToPersIdent;
    private String deliveredToPersRelativeLevel;
    private String deliveredToPersNote;
    private String deliveredToPersSignatureBase64;
    private String deliveredParcelimageBase64;
}
